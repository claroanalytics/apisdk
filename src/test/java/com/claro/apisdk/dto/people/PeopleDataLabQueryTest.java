package com.claro.apisdk.dto.people;

import org.junit.jupiter.api.Test;
import org.locationtech.jts.util.Assert;

class PeopleDataLabQueryTest {

    @Test
    public void should_not_throw_exceptions_on_empty() {
        PeopleDataLabQuery peopleDataLabQuery = new PeopleDataLabQuery();
        peopleDataLabQuery.buildQuery();
    }

    @Test
    public void should_not_throw_exceptions_on_set_null() {
        PeopleDataLabQuery peopleDataLabQuery = new PeopleDataLabQuery();
        peopleDataLabQuery.setFullname(null);
        peopleDataLabQuery.buildQuery();
    }

    @Test
    public void should_normalize_url() {
        Assert.equals(
                "linkedin.com/in/ivy-wan",
                PeopleDataLabQuery.normalizeUrl("https://www.linkedin.com/in/ivy-wan")
        );
        Assert.equals(
                "linkedin.com/in/ivy-wan",
                PeopleDataLabQuery.normalizeUrl("http://www.linkedin.com/in/ivy-wan")
        );
        Assert.equals(
                "linkedin.com/in/ivy-wan",
                PeopleDataLabQuery.normalizeUrl("www.linkedin.com/in/ivy-wan")
        );
        Assert.equals(
                "linkedin.com/in/ivy-wan",
                PeopleDataLabQuery.normalizeUrl("linkedin.com/in/ivy-wan")
        );
    }

}
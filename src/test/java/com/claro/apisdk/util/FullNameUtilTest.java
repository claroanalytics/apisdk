package com.claro.apisdk.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FullNameUtilTest {

    @Test
    public void testFullNameUtil() {
        String fullName = "John Snow";
        FullNameUtil fullNameUtil = new FullNameUtil(fullName);
        assertNotNull(fullNameUtil.getId());
        assertEquals("john", fullNameUtil.getName());
        assertEquals("snow", fullNameUtil.getSurname());

        String fullName1 = "John, Snow";
        FullNameUtil fullNameUtil1 = new FullNameUtil(fullName1);
        assertNotNull(fullNameUtil1.getId());
        assertEquals("john", fullNameUtil1.getName());
        assertEquals("snow", fullNameUtil1.getSurname());

        String fullName2 = "John, Snow.";
        FullNameUtil fullNameUtil2 = new FullNameUtil(fullName2);
        assertNotNull(fullNameUtil2.getId());
        assertEquals("john", fullNameUtil2.getName());
        assertEquals("snow", fullNameUtil2.getSurname());

        String fullName3 = "John,|/ Snow./:<";
        FullNameUtil fullNameUtil3 = new FullNameUtil(fullName3);
        assertNotNull(fullNameUtil3.getId());
        assertEquals("john", fullNameUtil3.getName());
        assertEquals("snow", fullNameUtil3.getSurname());

        String fullName4 = "";
        FullNameUtil fullNameUtil4 = new FullNameUtil(fullName4);
        assertNull(fullNameUtil4.getId());

        String fullName5 = "John";
        FullNameUtil fullNameUtil5 = new FullNameUtil(fullName5);
        assertNotNull(fullNameUtil5.getId());
        assertEquals("john", fullNameUtil5.getName());

        String fullName6 = "John,Snow";
        FullNameUtil fullNameUtil6 = new FullNameUtil(fullName6);
        assertNotNull(fullNameUtil6.getId());
        assertEquals("john", fullNameUtil6.getName());
        assertEquals("snow", fullNameUtil6.getSurname());

        String name7 = "John";
        String surname7 = null;
        FullNameUtil fullNameUtil7= new FullNameUtil(name7 + " " + surname7);
        assertNotNull(fullNameUtil7.getId());
        assertEquals("john", fullNameUtil7.getFullName());
        assertEquals("john", fullNameUtil7.getName());
        assertEquals("", fullNameUtil7.getSurname());

        String name8 = "İzzet GÜÇLÜ";
        FullNameUtil fullNameUtil8= new FullNameUtil(name8);
        assertNotNull(fullNameUtil8.getId());
        assertEquals("izzet guclu", fullNameUtil8.getFullName());
        assertEquals("izzet", fullNameUtil8.getName());
        assertEquals("guclu", fullNameUtil8.getSurname());

        assertEquals(fullNameUtil.getId(), fullNameUtil1.getId());
        assertEquals(fullNameUtil.getId(), fullNameUtil2.getId());
        assertEquals(fullNameUtil.getId(), fullNameUtil3.getId());
        assertEquals(fullNameUtil.getId(), fullNameUtil6.getId());
    }

}
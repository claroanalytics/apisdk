package com.claro.apisdk;

import com.claro.apisdk.util.RestTemplateUtil;
import com.data.document.elastic.AbstractCities;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Optional;

/**
 * Location Api.
 */
@Component
@Scope("prototype")
@RequiredArgsConstructor
@Slf4j
public class LocationApi {

    /**
     * Url path for location api.
     */
    @Value("${global.api.people.url:/api/location/less-filled-city}")
    private String path;

    /**
     * Rest api Config.
     */
    private final RestApiConfig restApiConfig;

    /**
     * Get less filled city.
     * @param countryCode
     * @param indexName
     * @return optional of AbstractCities
     */
    public Optional<AbstractCities> getLessFilledCity(
            final String countryCode,
            final String indexName
    ) throws RestClientException {
        String token = restApiConfig.getToken();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setBearerAuth(token);
        HttpEntity entity = new HttpEntity(httpHeaders);
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(restApiConfig.getHost() + path)
                .queryParam("countryCode", countryCode)
                .queryParam("indexName", indexName);
        final int connectionTimeout = 15 * 60_000;
        ResponseEntity<AbstractCities> responseEntity = RestTemplateUtil
                .getRestTemplate(connectionTimeout).exchange(
                        builder.build().toUriString(), HttpMethod.GET,
                        entity, AbstractCities.class
                );
        return Optional.ofNullable(responseEntity.getBody());
    }

}

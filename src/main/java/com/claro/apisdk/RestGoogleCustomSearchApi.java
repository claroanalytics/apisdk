package com.claro.apisdk;

import com.claro.apisdk.dto.google.CustomSearchRequest;
import com.claro.apisdk.dto.google.GoogleCustomSearch;
import com.claro.apisdk.util.RestTemplateUtil;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Scope;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Optional;

@Component
@Scope("prototype")
@RequiredArgsConstructor
public class RestGoogleCustomSearchApi {

    /**
     * Path.
     */
    private final String path = "/api/custom-search";

    /**
     * Rest api Config.
     */
    private final RestApiConfig restApiConfig;

    /**
     * Search.
     * @param customSearchRequest request
     * @return translated text
     */
    @SneakyThrows
    public GoogleCustomSearch search(
            final CustomSearchRequest customSearchRequest
    ) {
        String token = restApiConfig.getToken();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setBearerAuth(token);
        HttpEntity<?> entity = new HttpEntity<>(httpHeaders);
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(restApiConfig.getHost() + path)
                .queryParam("query", customSearchRequest.getQuery());
        Optional.ofNullable(customSearchRequest.getHighRange())
                .ifPresent(s -> builder.queryParam("highRange", s));
        Optional.ofNullable(customSearchRequest.getExactTerms())
                .ifPresent(s -> builder.queryParam("exactTerms", s));
        Optional.ofNullable(customSearchRequest.getSiteSearch())
                .ifPresent(s -> builder.queryParam("siteSearch", s));
        ResponseEntity<GoogleCustomSearch> responseEntity =
                RestTemplateUtil
                        .getRestTemplate()
                        .exchange(
                                builder.build().toUriString(),
                                HttpMethod.GET,
                                entity,
                                GoogleCustomSearch.class
                        );
        return responseEntity.getBody();
    }
}

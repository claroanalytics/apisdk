package com.claro.apisdk;

import com.claro.apisdk.util.FullNameUtil;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Rest gender api.
 */
@Component
@Scope("prototype")
public class RestGenderApi
        extends FullNameRequest
        implements IFullNameApi {

    /**
     * Fill gender.
     * @param fullNameUtils full name utils
     */
    @Override
    public void fill(final FullNameUtil[] fullNameUtils) {
        FullNameUtil[] responseArray = requestFill(fullNameUtils);
        for (FullNameUtil fullNameUtil : fullNameUtils) {
            for (FullNameUtil response : responseArray) {
                if (fullNameUtil.getId().equals(response.getId())) {
                    fullNameUtil.setGender(response.getGender());
                    break;
                }
            }
        }
    }

    /**
     * Fill gender.
     * @param fullNameUtil full name util
     */
    public void fill(final FullNameUtil fullNameUtil) {
        FullNameUtil[] fullNameUtils = new FullNameUtil[] {
                fullNameUtil
        };
        fill(fullNameUtils);
    }

    /**
     * Fill gender.
     * @param fullNameUtils full name utils
     * @return array updated full names
     */
    @Override
    public FullNameUtil[] updateLinkedin(
            final FullNameUtil[] fullNameUtils) {
        return requestUpdateLinkedin(fullNameUtils);
    }

    /**
     * Get fill path.
     * @return string
     */
    @Override
    public String getFillPath() {
        return "/api/gender/fill";
    }

    /**
     * Get update linkedin path.
     * @return string
     */
    @Override
    public String getUpdateLinkedinPath() {
        return "/api/gender/updateLinkedin";
    }
}

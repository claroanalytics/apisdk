package com.claro.apisdk;

import com.claro.apisdk.dto.GoogleGeoLoc;
import com.claro.apisdk.dto.google.GooglePlace;
import com.claro.apisdk.dto.google.GooglePlacesRequest;
import com.claro.apisdk.util.RestTemplateUtil;
import com.data.document.elastic.Loc;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Google Geo Api.
 */
@Component
@Scope("prototype")
@RequiredArgsConstructor
public class RestGoogleGeoApi {

    /**
     * Url path for location.
     */
    @Value("${global.api.geo.location.url:/api/geo/location/}")
    private String locationPath;

    /**
     * Url path for loc.
     */
    @Value("${global.api.geo.loc.url:/api/geo/loc/}")
    private String locPath;

    /**
     * Url path for coordinates loc.
     */
    @Value("${global.api.geo.coordinates.loc.url:/api/geo/coordinates/loc/}")
    private String coordinatesLocPath;

    /**
     * Url path for convert to loc.
     */
    @Value("${global.api.geo.convertToLoc.url:/api/geo/convertToLoc/}")
    private String convertToLocPath;

    /**
     * Url path for places.
     */
    @Value("${global.api.geo.places.url:/api/geo/places/}")
    private String placesPath;

    /**
     * ExternalApiConnector Config.
     */
    private final RestApiConfig restApiConfig;

    /**
     * Retry template.
     */
    private final RetryTemplate retryTemplate;


    /**
     * Get GoogleGeoLoc by countryCode and address String.
     * @param location
     * @param countryCode
     * @return GoogleGeoLoc
     */
    public GoogleGeoLoc getGoogleGeoLoc(final String location,
            final String countryCode) {
        String token = restApiConfig.getToken();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setBearerAuth(token);
        HttpEntity entity = new HttpEntity(httpHeaders);
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(restApiConfig.getHost() + locationPath)
                .queryParam("location", location)
                .queryParam("countryCode", countryCode);

        ResponseEntity<GoogleGeoLoc> responseEntity = retryTemplate
                .execute(retryContext -> RestTemplateUtil
                        .getRestTemplate().exchange(
                                builder.build().toUriString(),
                                HttpMethod.GET,
                                entity,
                                GoogleGeoLoc.class
                        ));
        return responseEntity.getBody();
    }

    /**
     * Refresh and return mew google geo location.
     * @param location
     * @param countryCode
     * @return GoogleGeoLoc
     */
    public GoogleGeoLoc refreshGoogleGeoLoc(final String location,
            final String countryCode) {
        String token = restApiConfig.getToken();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setBearerAuth(token);
        HttpEntity entity = new HttpEntity(httpHeaders);
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(restApiConfig.getHost() + locationPath)
                .queryParam("location", location)
                .queryParam("countryCode", countryCode);

        ResponseEntity<GoogleGeoLoc> responseEntity = retryTemplate
                .execute(retryContext -> RestTemplateUtil
                        .getRestTemplate().exchange(
                                builder.build().toUriString(),
                                HttpMethod.POST,
                                entity,
                                GoogleGeoLoc.class
                        ));
        return responseEntity.getBody();
    }


    /**
     * Get Elastic entity Loc by countryCode and address String.
     * @param location
     * @param countryCode
     * @return GoogleGeoLoc
     */
    public Loc getLoc(final String location,
            final String countryCode) {
        String token = restApiConfig.getToken();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setBearerAuth(token);
        HttpEntity entity = new HttpEntity(httpHeaders);
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(restApiConfig.getHost() + locPath)
                .queryParam("location", location)
                .queryParam("countryCode", countryCode);

        ResponseEntity<Loc> responseEntity = retryTemplate
                .execute(retryContext -> RestTemplateUtil
                        .getRestTemplate().exchange(
                                builder.build().toUriString(),
                                HttpMethod.GET,
                                entity,
                                Loc.class
                        ));
        return responseEntity.getBody();
    }

    /**
     * Get loc by coordinates.
     * @param lat latitude
     * @param lon longitude
     * @return loc
     */
    public Loc getLocByCoordinates(
        final double lat,
        final double lon
    ) {
        String token = restApiConfig.getToken();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setBearerAuth(token);
        UriComponentsBuilder builder = UriComponentsBuilder
            .fromHttpUrl(restApiConfig.getHost() + coordinatesLocPath)
            .queryParam("lat", lat)
            .queryParam("lon", lon);
        return RestTemplateUtil
            .getRestTemplate().exchange(
                builder.build().toUriString(),
                HttpMethod.GET,
                new HttpEntity<>(httpHeaders),
                Loc.class
            ).getBody();
    }

    /**
     * Convert to loc.
     * @param googleGeoLoc
     * @return GoogleGeoLoc
     */
    public Loc convertToLoc(final GoogleGeoLoc googleGeoLoc) {
        String token = restApiConfig.getToken();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setBearerAuth(token);
        HttpEntity entity = new HttpEntity(googleGeoLoc, httpHeaders);
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(restApiConfig.getHost() + convertToLocPath);

        ResponseEntity<Loc> responseEntity = retryTemplate
                .execute(retryContext -> RestTemplateUtil
                        .getRestTemplate().exchange(
                                builder.build().toUriString(),
                                HttpMethod.POST,
                                entity,
                                Loc.class
                        ));
        return responseEntity.getBody();
    }

    /**
     * Get Google places autocomplete.
     * @param input
     * @return list of places description
     */
    public List<String> getGooglePlaces(final String input) {
        return getGooglePlacesExt(input).stream()
                .map(p -> p.getAutocompletePrediction().description)
                .collect(Collectors.toList());
    }

    /**
     * Get Google places autocomplete extended.
     * @param input
     * @return list of GooglePlace
     */
    public List<GooglePlace> getGooglePlacesExt(final String input) {
        return findGooglePlacesExt(new GooglePlacesRequest().setInput(input));
    }

    /**
     * Find Google places autocomplete extended with optional params.
     *
     * @param request
     *
     * @return list of GooglePlace
     */
    public List<GooglePlace> findGooglePlacesExt(
            final GooglePlacesRequest request) {
        String token = restApiConfig.getToken();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setBearerAuth(token);
        HttpEntity entity = new HttpEntity(request, httpHeaders);

        UriComponentsBuilder builder = UriComponentsBuilder
            .fromHttpUrl(restApiConfig.getHost() + placesPath);
        ResponseEntity<List<GooglePlace>> responseEntity
            = RestTemplateUtil.getRestTemplate().exchange(
            builder.build().toUriString(), HttpMethod.POST, entity,
                new ParameterizedTypeReference<List<GooglePlace>>() { });
        return responseEntity.getBody();
    }
}

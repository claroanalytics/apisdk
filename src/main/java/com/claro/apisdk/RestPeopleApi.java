package com.claro.apisdk;

import com.claro.apisdk.dto.people.ContactDiscoveryStatus;
import com.claro.apisdk.dto.people.EmailVerificationQuery;
import com.claro.apisdk.dto.people.PeopleDataLabQuery;
import com.claro.apisdk.dto.people.PeopleQuery;
import com.claro.apisdk.dto.people.PeopleQueryResult;
import com.claro.apisdk.dto.people.VerificationResult;
import com.claro.apisdk.dto.people.email.PeopleEmailsDTO;
import com.claro.apisdk.dto.rabbit.SocialNetworkType;
import com.claro.apisdk.util.RestTemplateUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * People Api.
 */
@Component
@Scope("prototype")
@RequiredArgsConstructor
@Slf4j
public class RestPeopleApi {

    /**
     * Social link response.
     */
    private static final ParameterizedTypeReference<HashMap
        <SocialNetworkType, String>>
        SOCIAL_LINK_RESPONSE =
        new ParameterizedTypeReference<HashMap<SocialNetworkType, String>>() {
        };

    /**
     * Url path for people api.
     */
    @Value("${global.api.people.url:/api/people/}")
    private String path;

    /**
     * Url path for email verification.
     */
    @Value("${global.api.people.url:/api/people/verify-email/}")
    private String emailPath;

    /**
     * Rest api Config.
     */
    private final RestApiConfig restApiConfig;

    /**
     * ObjectMapper.
     */
    private final ObjectMapper objectMapper;

    /**
     * Query people.
     *
     * @param query the query data
     * @return list of people info.
     */
    @SneakyThrows
    public List<PeopleQueryResult> query(final PeopleQuery query) {
        String token = restApiConfig.getToken();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setBearerAuth(token);
        String json = objectMapper.writeValueAsString(query);
        HttpEntity<String> entity = new HttpEntity<>(json, httpHeaders);
        try {
            ResponseEntity<List<PeopleQueryResult>> responseEntity
                    = RestTemplateUtil.getRestTemplate().exchange(
                            restApiConfig.getHost() + path, HttpMethod.POST,
                            entity, new ParameterizedTypeReference
                            <List<PeopleQueryResult>>() { });
                return responseEntity.getBody();
        } catch (HttpServerErrorException | HttpClientErrorException e) {
            log.error("", e);
        }
        return Collections.emptyList();
    }

    /**
     * Search people.
     *
     * @param query the query data
     * @return list of people info.
     */
    @SneakyThrows
    public ResponseEntity<String> search(final PeopleDataLabQuery query) {
        String token = restApiConfig.getToken();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setBearerAuth(token);
        String json = objectMapper.writeValueAsString(query);
        HttpEntity<String> entity = new HttpEntity<>(json, httpHeaders);
        String url = UriComponentsBuilder.fromUriString(restApiConfig.getHost())
                .path(path)
                .pathSegment("search")
                .toUriString();
        return RestTemplateUtil.getRestTemplate().exchange(
                    url,
                    HttpMethod.POST,
                    entity,
                    String.class
            );
    }

    /**
     * Email verification.
     * @param query
     * @return verification result
     */
    @SneakyThrows
    public VerificationResult verifyEmail(final EmailVerificationQuery query) {
        String token = restApiConfig.getToken();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setBearerAuth(token);
        String json = objectMapper.writeValueAsString(query);
        HttpEntity<String> entity = new HttpEntity<>(json, httpHeaders);
        try {
            ResponseEntity<VerificationResult> responseEntity = RestTemplateUtil
                    .getRestTemplate().exchange(
                            restApiConfig.getHost() + emailPath,
                            HttpMethod.POST,
                            entity, VerificationResult.class);
            return responseEntity.getBody();
        } catch (HttpServerErrorException | HttpClientErrorException e) {
            log.error("", e);
        }
        return null;
    }

    /**
     * Discover email information for talent.
     *
     * @param talentId talent to operate on
     * @return discovery operation status
     */
    @SneakyThrows
    public ContactDiscoveryStatus discoverEmail(final String talentId) {
        return discoverEmail(talentId, false);
    }

    /**
     * Discover email information for talent.
     *
     * @param talentId talent to operate on
     * @param priority priority (default used false)
     * @return discovery operation status
     */
    @SneakyThrows
    public ContactDiscoveryStatus discoverEmail(final String talentId, final
        boolean priority) {
        String token = restApiConfig.getToken();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(token);
        HttpEntity httpEntity = new HttpEntity(httpHeaders);
        String discoverEmailUrl = buildDiscoverEmailUrl(talentId, priority);
        try {
            ResponseEntity<ContactDiscoveryStatus> responseEntity =
                    RestTemplateUtil.getRestTemplate().exchange(
                            discoverEmailUrl,
                            HttpMethod.GET,
                            httpEntity,
                            ContactDiscoveryStatus.class
                    );
            return responseEntity.getBody();
        } catch (HttpServerErrorException | HttpClientErrorException e) {
            log.error("talentId:{} url:{}", talentId, discoverEmailUrl, e);
        }
        return ContactDiscoveryStatus.FAILED;
    }

    /**
     * Return found email for talent id.
     *
     * @param talentId identifier of talent to search email for
     * @return PeopleDTO with email found
     */
    public PeopleEmailsDTO lookupEmail(final String talentId) {
        String token = restApiConfig.getToken();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(token);
        HttpEntity httpEntity = new HttpEntity(httpHeaders);
        try {
            ResponseEntity<PeopleEmailsDTO> responseEntity = RestTemplateUtil
                    .getRestTemplate().exchange(
                            buildLookupEmailUrl(talentId),
                            HttpMethod.GET,
                    httpEntity, PeopleEmailsDTO.class);
            return responseEntity.getBody();
        } catch (HttpServerErrorException | HttpClientErrorException e) {
            log.error("lookupEmail", e);
            throw e;
        }
    }

    /**
     * Return found social links for talent id.
     *
     * @param talentId identifier of talent to search email for
     * @return social links
     */
    public HashMap<SocialNetworkType, String> lookupSocialLinks(
        final String talentId) {
        String token = restApiConfig.getToken();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(token);
        HttpEntity httpEntity = new HttpEntity(httpHeaders);
        try {
            ResponseEntity<HashMap<SocialNetworkType, String>> responseEntity =
                RestTemplateUtil
                    .getRestTemplate().exchange(
                        buildLookupSocialLinksUrl(talentId), HttpMethod.GET,
                        httpEntity, SOCIAL_LINK_RESPONSE);
            return responseEntity.getBody();
        } catch (HttpServerErrorException | HttpClientErrorException e) {
            log.error("lookupSocialLinks", e);
            throw e;
        }
    }

    private String buildDiscoverEmailUrl(final String talentId, final
        Boolean priority) {
        return UriComponentsBuilder.fromUriString(restApiConfig.getHost())
                .path(path)
                .pathSegment("discover-contact", talentId)
                .queryParam("priority", priority)
                .toUriString();
    }

    private String buildLookupEmailUrl(final String talentId) {
        return UriComponentsBuilder.fromUriString(restApiConfig.getHost())
            .path(path)
            .pathSegment("lookup-email", talentId)
            .toUriString();
    }

    private String buildLookupSocialLinksUrl(final String talentId) {
        return UriComponentsBuilder.fromUriString(restApiConfig.getHost())
            .path(path)
            .pathSegment("lookup-social-links", talentId)
            .toUriString();
    }

}

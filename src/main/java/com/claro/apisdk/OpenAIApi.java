package com.claro.apisdk;

import com.claro.apisdk.dto.chatgpt.OpenAIDTORequest;
import com.claro.apisdk.util.RestTemplateUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

@Service
@RequiredArgsConstructor
@Slf4j
public class OpenAIApi {

    /**
     * Url path for chat gpt api.
     */
    private String path = "/api/openai/chat";
    /**
     * Rest api Config.
     */
    private final RestApiConfig restApiConfig;
    /**
     * Retry template.
     */
    private final RetryTemplate retryTemplate;

    /**
     * Generate chat response.
     *
     * @param request OpenAI DTO request.
     * @return String response.
     */
    public String generateChatResponse(
            final OpenAIDTORequest request) {
        String token = restApiConfig.getToken();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setBearerAuth(token);
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(restApiConfig.getHost() + path);
        HttpEntity<OpenAIDTORequest> requestEntity = new HttpEntity<>(
                request, httpHeaders);
        ResponseEntity<String> response = retryTemplate
                .execute(retryContext ->
                        RestTemplateUtil.getRestTemplate()
                                .exchange(
                                        builder.toUriString(),
                                        HttpMethod.POST,
                                        requestEntity,
                                        String.class
                                )
                );
        return response.getBody();
    }
}

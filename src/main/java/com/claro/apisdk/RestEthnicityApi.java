package com.claro.apisdk;

import com.claro.apisdk.util.FullNameUtil;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Rest ethnicity api.
 */
@Component
@Scope("prototype")
public class RestEthnicityApi
        extends FullNameRequest
        implements IFullNameApi {

    /**
     * Fill ethnicity.
     * @param fullNameUtils full name utils
     */
    @Override
    public void fill(final FullNameUtil[] fullNameUtils) {
        FullNameUtil[] responseArray = requestFill(fullNameUtils);
        for (FullNameUtil fullNameUtil : fullNameUtils) {
            for (FullNameUtil response : responseArray) {
                if (fullNameUtil.getId().equals(response.getId())) {
                    fullNameUtil.setEthnicity(response.getEthnicity());
                    break;
                }
            }
        }
    }

    /**
     * Update linkedin.
     * @param fullNameUtils full name utils
     * @return array updated full names
     */
    @Override
    public FullNameUtil[] updateLinkedin(final FullNameUtil[] fullNameUtils) {
        return requestUpdateLinkedin(fullNameUtils);
    }

    /**
     * Get path.
     * @return string
     */
    @Override
    public String getFillPath() {
        return "/api/ethnicity/fill";
    }

    /**
     * Get update linkedin path.
     * @return string
     */
    @Override
    public String getUpdateLinkedinPath() {
        return "/api/ethnicity/updateLinkedin";
    }
}

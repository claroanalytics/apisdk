package com.claro.apisdk;

import com.claro.apisdk.util.FullNameUtil;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Rest diaspora api.
 */
@Component
@Scope("prototype")
public class RestDiasporaApiV2
        extends FullNameRequest
        implements IFullNameApi {

    /**
     * Fill diaspora.
     * @param fullNameUtils full name utils
     */
    @Override
    public void fill(final FullNameUtil[] fullNameUtils) {
        FullNameUtil[] responseArray = requestFill(fullNameUtils);
        for (FullNameUtil fullNameUtil : fullNameUtils) {
            for (FullNameUtil response : responseArray) {
                if (fullNameUtil.getId().equals(response.getId())) {
                    fullNameUtil.setDiaspora(response.getDiaspora());
                    break;
                }
            }
        }
    }

    /**
     * Update linkedin.
     * @param fullNameUtils full name utils
     * @return array updated full names
     */
    @Override
    public FullNameUtil[] updateLinkedin(final FullNameUtil[] fullNameUtils) {
        return requestUpdateLinkedin(fullNameUtils);
    }

    /**
     * Get path.
     * @return string
     */
    @Override
    public String getFillPath() {
        return "/api/diaspora/fill-up";
    }

    /**
     * Get update linkedin path.
     * @return string
     */
    @Override
    public String getUpdateLinkedinPath() {
        return "/api/diaspora/update-linkedin";
    }
}

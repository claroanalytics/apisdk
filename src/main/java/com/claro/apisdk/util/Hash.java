package com.claro.apisdk.util;


import org.apache.commons.codec.binary.Hex;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Hash string util.
 */
public class Hash {

    /**
     * Hash string to md5.
     * @param input Send string.
     *
     * @return String hashed with md5 algorithm.
     */
    public String md5String(final String input) {
        String output = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(input.getBytes());
            byte[] digest = md.digest();
            output = Hex.encodeHexString(digest).toUpperCase();
        } catch (NoSuchAlgorithmException e) {
            System.out.println("String to MD5 hash problem.");
        }

        return output;
    }
}

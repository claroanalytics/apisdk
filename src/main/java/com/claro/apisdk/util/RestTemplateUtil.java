package com.claro.apisdk.util;

import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * Rest template util.
 */
public final class RestTemplateUtil {

    /**
     * Private.
     */
    private RestTemplateUtil() { }

    /**
     * Get rest template.
     *
     * @return rest template
     */
    public static RestTemplate getRestTemplate() {
        final int defaultTimeout = 3 * 60_000;
        return getRestTemplate(defaultTimeout);
    }

    /**
     * Get rest template.
     *
     * @param connectionTimeout connection timeout
     * @return rest template
     */
    public static RestTemplate getRestTemplate(final int connectionTimeout) {
        RequestConfig requestConfig =
                RequestConfig.custom()
                        .setCookieSpec(CookieSpecs.STANDARD_STRICT)
                        .setConnectTimeout(connectionTimeout)
                        .build();
        ClientHttpRequestFactory requestFactory =
                new HttpComponentsClientHttpRequestFactory(
                        HttpClients.custom()
                                .setDefaultRequestConfig(requestConfig)
                                .build()
                );
        return new RestTemplate(requestFactory);
    }
}

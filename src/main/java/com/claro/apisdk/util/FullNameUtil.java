package com.claro.apisdk.util;

import static org.elasticsearch.index.query.QueryBuilders.matchQuery;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

/**
 * Full name util.
 */
@Data
public class FullNameUtil {

    /**
     * Id.
     */
    @Getter
    private String id;

    /**
     * FullName.
     */
    @Getter
    @Setter
    private String fullName;

    /**
     * fullName Original.
     */
    @Getter
    @Setter
    private String fullNameOriginal;

    /**
     * Name.
     */
    private String name;

    /**
     * Surname.
     */
    private String surname;

    /**
     * Gender.
     */
    @Getter
    @Setter
    private String gender;

    /**
     * Gender refilled.
     */
    @Getter
    @Setter
    private Boolean genderRefilled;

    /**
     * Ethnicity.
     */
    @Getter
    @Setter
    private String ethnicity;

    /**
     * Ethnicity refilled.
     */
    @Getter
    @Setter
    private Boolean ethnicityRefilled;

    /**
     * Diaspora.
     */
    @Getter
    @Setter
    private String diaspora;

    /**
     * Diaspora refilled.
     */
    @Getter
    @Setter
    private Boolean diasporaRefilled;

    /**
     * Country code from platform request.
     */
    @Getter
    @Setter
    private String countryCode;

    /**
     * Country code from api response.
     */
    @Getter
    @Setter
    private String responseCountryCode;

    /**
     * Constructor.
     */
    public FullNameUtil() {
    }

    /**
     * Constructor.
     *
     * @param fullNameArg full name
     */
    public FullNameUtil(final String fullNameArg) {
        fullNameUtil(fullNameArg, null);
    }

    /**
     * Constructor.
     *
     * @param fullNameArg full name
     * @param code        country code
     */
    public FullNameUtil(final String fullNameArg, final String code) {
        fullNameUtil(fullNameArg, code);
    }

    private void fullNameUtil(final String fullNameArg,
            final String code) {
        fullNameOriginal = fullNameArg;
        if (fullNameArg == null || fullNameArg.isEmpty()) {
            return;
        }
        if (code != null) {
            countryCode = code;
        }
        fullName = fullNameArg
                .replaceAll("\\P{L}+", " ")
                .replace("null", "")
                .replaceAll("\\s+", " ").toLowerCase().trim();
        fullName = StringUtils.stripAccents(fullName);
        String[] splitFullName = fullName.split(" ");
        if (splitFullName.length > 0) {
            name = splitFullName[0].toLowerCase();
            surname = "";
            if (splitFullName.length >= 2) {
                surname = splitFullName[1].toLowerCase();
            }
            id = new Hash().md5String((name + " " + surname).trim());
        }
    }

    /**
     * Getter.
     *
     * @return name
     */
    public String getName() {
        if (name == null) {
            name = "";
        }
        return name;
    }

    /**
     * Getter.
     *
     * @return name
     */
    public String getSurname() {
        if (surname == null) {
            surname = "";
        }
        return surname;
    }

    /**
     * Get fullname linkedin query.
     *
     * @param fullName    fullname
     * @param countryCode countryCode
     * @return query
     */
    public static QueryBuilder getFullNameLinkedinQuery(
            final String fullName, final String countryCode
    ) {
        FullNameUtil fullNameUtil = new FullNameUtil(fullName);
        BoolQueryBuilder fullNameBool = new BoolQueryBuilder();
        if (countryCode != null) {
            fullNameBool.must(matchQuery("loc.country_code", countryCode));
        }
        if (fullNameUtil.getSurname() != null
                && !fullNameUtil.getSurname().isEmpty()
        ) {
            fullNameBool.should(QueryBuilders
                    .matchQuery(
                            "fullname.keywordWord",
                            fullNameUtil.getFullName()
                    ));
            fullNameBool.should(QueryBuilders
                    .wildcardQuery(
                            "fullname.keywordWord",
                            fullNameUtil.getFullName() + " *"
                    ));
            fullNameBool.should(QueryBuilders
                    .wildcardQuery(
                            "fullname.keywordWord",
                            " " + fullNameUtil.getFullName() + " *"
                    ));
        } else {
            fullNameBool.should(QueryBuilders
                    .matchQuery(
                            "fullname.keywordWord",
                            fullNameUtil.getFullName()
                    ));
            fullNameBool.should(QueryBuilders
                    .matchQuery(
                            "fullname.keywordWord",
                            fullNameUtil.getFullName() + " "
                    ));
            fullNameBool.should(QueryBuilders
                    .matchQuery(
                            "fullname.keywordWord",
                            " " + fullNameUtil.getFullName() + " "
                    ));
        }
        return fullNameBool;
    }
}

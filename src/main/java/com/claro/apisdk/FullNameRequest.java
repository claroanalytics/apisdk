package com.claro.apisdk;

import com.claro.apisdk.util.FullNameUtil;
import com.claro.apisdk.util.RestTemplateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;

/**
 * Full name request.
 */
public abstract class FullNameRequest {

    /**
     * Global rest api config.
     */
    @Autowired
    private RestApiConfig restApiConfig;

    /**
     * Request.
     * @param fullNameUtils full name utils
     * @return list
     * @throws HttpClientErrorException e
     */
    protected FullNameUtil[] requestFill(final FullNameUtil[] fullNameUtils)
            throws HttpClientErrorException {
        String token = restApiConfig.getToken();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setBearerAuth(token);
        HttpEntity<FullNameUtil[]> entity =
                new HttpEntity<>(fullNameUtils, httpHeaders);
        ResponseEntity<FullNameUtil[]> responseEntity =
                RestTemplateUtil
                        .getRestTemplate()
                        .exchange(
                                restApiConfig.getHost() + getFillPath(),
                                HttpMethod.POST,
                                entity,
                                FullNameUtil[].class
                        );
        return responseEntity.getBody();
    }

    /**
     * Request.
     * @param fullNameUtils full name utils
     * @return array updated full names
     * @throws HttpClientErrorException e
     */
    protected FullNameUtil[] requestUpdateLinkedin(
            final FullNameUtil[] fullNameUtils)
            throws HttpClientErrorException {
        String token = restApiConfig.getToken();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setBearerAuth(token);
        HttpEntity<FullNameUtil[]> entity =
                new HttpEntity<>(fullNameUtils, httpHeaders);
        return RestTemplateUtil.getRestTemplate().exchange(
                restApiConfig.getHost() + getUpdateLinkedinPath(),
                HttpMethod.POST,
                entity,
                FullNameUtil[].class
        ).getBody();
    }

    /**
     * Get fill path.
     * @return string
     */
    public abstract String getFillPath();

    /**
     * Get update linkedin path.
     * @return string
     */
    public abstract String getUpdateLinkedinPath();
}

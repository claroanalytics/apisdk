package com.claro.apisdk;

import com.claro.apisdk.dto.IpAddressLocation;
import com.claro.apisdk.util.RestTemplateUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Rest ip address api.
 */
@Component
@Scope("prototype")
@RequiredArgsConstructor
@Slf4j
public class RestIpAddressApi {

    /**
     * Rest api Config.
     */
    private final RestApiConfig restApiConfig;

    /**
     * Find location.
     * @param ip ip
     * @return location
     */
    public IpAddressLocation findLocation(
            final String ip
    ) throws RestClientException {
        String token = restApiConfig.getToken();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setBearerAuth(token);
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(restApiConfig.getHost()
                        + "/api/ip-address/location"
                );
        builder.queryParam("ipAddress", ip);
        ResponseEntity<IpAddressLocation> responseEntity = RestTemplateUtil
                .getRestTemplate().exchange(
                        builder.build().toUriString(),
                        HttpMethod.GET,
                        new HttpEntity<>(httpHeaders),
                        IpAddressLocation.class
                );
        return responseEntity.getBody();
    }
}

package com.claro.apisdk;

import com.claro.apisdk.util.FullNameUtil;

/**
 * Full name api interface.
 */
public interface IFullNameApi {

    /**
     * Fill.
     * @param fullNameUtils full name utils
     */
    void fill(FullNameUtil[] fullNameUtils);

    /**
     * Update linkedin.
     * @param fullNameUtils full name utils
     * @return array updated full names
     */
    FullNameUtil[] updateLinkedin(FullNameUtil[] fullNameUtils);
}

package com.claro.apisdk;

import com.claro.apisdk.util.RestTemplateUtil;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * Google Translate Api.
 */
@Component
@Scope("prototype")
@RequiredArgsConstructor
public class RestGoogleTranslateApi {

    /**
     * Url path for location.
     */
    @Value("${global.api.translate.text.url:/api/translate}")
    private String translatePath;

    /**
     * Rest api Config.
     */
    private final RestApiConfig restApiConfig;

    /**
     * Translate text from any language to English.
     * @param text
     * @return translated text
     */
    @SneakyThrows
    public String translateTextToEnglish(final String text) {
        String token = restApiConfig.getToken();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setBearerAuth(token);
        HttpEntity entity = new HttpEntity(httpHeaders);
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(restApiConfig.getHost() + translatePath)
                .queryParam("text", URLEncoder.encode(text,
                        StandardCharsets.UTF_8.toString()));

        ResponseEntity<String> responseEntity = RestTemplateUtil
                .getRestTemplate().exchange(
                        builder.build().toUriString(), HttpMethod.GET,
                        entity, String.class);
        return responseEntity.getBody();
    }
}

package com.claro.apisdk;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.claro.apisdk.dto.openai.OpenAiStreamRequest;
import com.claro.apisdk.dto.openai.OpenAiStreamResponse;
import com.claro.apisdk.util.RestTemplateUtil;

/**
 * Open AI stream api.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class OpenAiStreamApi {

  /**
   * Url path for open ai stream.
   */
  private static final String PATH = "/api/openai/stream";

  /**
   * Rest api Config.
   */
  private final RestApiConfig restApiConfig;

  /**
   * Generate open ai text.
   *
   * @param request OpenAI DTO request.
   * @return String response.
   */
  public OpenAiStreamResponse generate(final OpenAiStreamRequest request) {
    String token = restApiConfig.getToken();
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    httpHeaders.setBearerAuth(token);
    UriComponentsBuilder builder = UriComponentsBuilder
        .fromHttpUrl(restApiConfig.getHost() + PATH);
    HttpEntity<OpenAiStreamRequest> requestEntity =
        new HttpEntity<>(request, httpHeaders);
    ResponseEntity<OpenAiStreamResponse> response = RestTemplateUtil
        .getRestTemplate()
        .exchange(
            builder.toUriString(),
            HttpMethod.POST,
            requestEntity,
            OpenAiStreamResponse.class
        );
    return response.getBody();
  }
}

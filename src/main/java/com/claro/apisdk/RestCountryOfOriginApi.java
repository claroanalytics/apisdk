package com.claro.apisdk;

import com.claro.apisdk.util.FullNameUtil;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Rest country of origin api.
 */
@Component
@Scope("prototype")
public class RestCountryOfOriginApi
        extends FullNameRequest
        implements IFullNameApi {

    /**
     * Fill data.
     * @param fullNameUtils full name utils
     */
    @Override
    public void fill(final FullNameUtil[] fullNameUtils) {
        FullNameUtil[] responseArray = requestFill(fullNameUtils);
        for (FullNameUtil fullNameUtil : fullNameUtils) {
            for (FullNameUtil response : responseArray) {
                if (fullNameUtil.getId().equals(response.getId())) {
                    fullNameUtil.setResponseCountryCode(
                            response.getResponseCountryCode()
                    );
                    break;
                }
            }
        }
    }

    /**
     * Update linkedin.
     * @param fullNameUtils full name utils
     * @return array updated full names
     */
    @Override
    public FullNameUtil[] updateLinkedin(final FullNameUtil[] fullNameUtils) {
        return requestUpdateLinkedin(fullNameUtils);
    }

    /**
     * Get path.
     * @return string
     */
    @Override
    public String getFillPath() {
        return "/api/country-of-origin/fill";
    }

    /**
     * Get update linkedin path.
     * @return string
     */
    @Override
    public String getUpdateLinkedinPath() {
        return "/api/country-of-origin/update-linkedin";
    }
}

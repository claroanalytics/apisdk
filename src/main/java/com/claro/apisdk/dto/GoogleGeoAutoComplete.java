package com.claro.apisdk.dto;

import com.google.maps.model.AutocompletePrediction;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * Google geo autocomplete cache in elastic.
 */
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@Document(indexName = "google_geo_autocomplete")
public class GoogleGeoAutoComplete {

    /**
     * Id.
     */
    @Id
    private String id;

    /**
     * Autocomplete Query.
     */
    @Field(type = FieldType.Keyword)
    private String query;

    /**
     * Google Geo Autocomplete response.
     */
    private AutocompletePrediction[] response;
}

package com.claro.apisdk.dto.openai;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Open ai stream request.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OpenAiStreamRequest {

  /**
   * Assistant id.
   */
  private String assistantId;

  /**
   * Message.
   */
  private String message;
}

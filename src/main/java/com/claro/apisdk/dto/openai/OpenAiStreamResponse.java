package com.claro.apisdk.dto.openai;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Open ai stream response.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OpenAiStreamResponse {

  /**
   * Content.
   */
  private String content;

}


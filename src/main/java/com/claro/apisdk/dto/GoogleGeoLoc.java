package com.claro.apisdk.dto;

import com.google.maps.model.GeocodingResult;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.GeoPointField;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Google geo loc.
 */
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@Document(indexName = "google_geo_loc")
public class GoogleGeoLoc {

    /**
     * Id.
     */
    @Id
    private String id;

    /**
     * Location.
     */
    private String location;

    /**
     * Country code.
     */
    @Getter
    @Setter
    private String countryCode;

    /**
     * Geo coding result.
     */
    private GeocodingResult geocodingResult;

    /**
     * Added time.
     */
    @Field(type = FieldType.Date)
    private Date addedTime;

    /**
     * Change log.
     */
    private Map<String, String> changeLog = new HashMap<>();

    /**
     * Refilled.
     */
    private Boolean refilled = false;

    /**
     * GeoPoint.
     */
    @GeoPointField
    private GeoPoint geoPoint;
}

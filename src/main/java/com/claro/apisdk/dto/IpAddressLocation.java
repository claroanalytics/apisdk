package com.claro.apisdk.dto;

import lombok.Data;

@Data
public class IpAddressLocation {
    /**
     * Ip.
     */
    private String ip;
    /**
     * City.
     */
    private String city;
    /**
     * Region code.
     */
    private String regionCode;
    /**
     * Country code.
     */
    private String countryCode;
    /**
     * Lat.
     */
    private Double lat;
    /**
     * Lon.
     */
    private Double lon;
}

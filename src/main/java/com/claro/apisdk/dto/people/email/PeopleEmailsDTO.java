package com.claro.apisdk.dto.people.email;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * People emails.
 *
 * @author Sergey Parfenov
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PeopleEmailsDTO {

    /**
     * Emails.
     */
    @Default
    private List<PeopleEmailDTO> emails = new ArrayList<>();

    /**
     * Work emails.
     */
    @Default
    private List<PeopleEmailDTO> workEmails = new ArrayList<>();

    /**
     * Exist not found result.
     */
    private boolean existNotFoundResult;

    /**
     * Need update contacts.
     */
    private boolean needUpdate;
}

package com.claro.apisdk.dto.people;

import lombok.Data;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.springframework.data.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static org.elasticsearch.index.query.QueryBuilders.termQuery;
import static org.springframework.util.StringUtils.hasLength;

@Data
public class PeopleDataLabQuery {

    /**
     * Fullname.
     */
    private String fullname;
    /**
     * Linkedin url.
     */
    private String linkedinUrl;
    /**
     * Facebook url.
     */
    private String facebookUrl;
    /**
     * Twitter url.
     */
    private String twitterUrl;
    /**
     * Github url.
     */
    private String githubUrl;
    /**
     * Job title.
     */
    private String jobTitle;
    /**
     * Work email.
     */
    private String workEmail;
    /**
     * Personal email.
     */
    private String personalEmail;
    /**
     * Company name.
     */
    private String companyName;
    /**
     * Location.
     */
    private String location;
    /**
     * Skill.
     */
    private String skill;
    /**
     * Education.
     */
    private String education;

    /**
     * Build query.
     * @return query
     */
    public QueryBuilder buildQuery() {
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        List<Pair<String, Function<String, QueryBuilder>>> list =
                new ArrayList<>();
        list.add(buildPair(
                fullname,
                PeopleDataLabQuery::fullNameQuery
        ));
        list.add(buildPair(
                linkedinUrl,
                PeopleDataLabQuery::linkedinUrlQuery
        ));
        list.add(buildPair(
                facebookUrl,
                PeopleDataLabQuery::facebookUrlQuery
        ));
        list.add(buildPair(
                twitterUrl,
                PeopleDataLabQuery::twitterUrlQuery
        ));
        list.add(buildPair(
                githubUrl,
                PeopleDataLabQuery::githubUrlQuery
        ));
        list.add(buildPair(
                jobTitle,
                PeopleDataLabQuery::jobTitleQuery
        ));
        list.add(buildPair(
                workEmail,
                PeopleDataLabQuery::workEmailQuery
        ));
        list.add(buildPair(
                personalEmail,
                PeopleDataLabQuery::personalEmailQuery
        ));
        list.add(buildPair(
                companyName,
                PeopleDataLabQuery::companyNameQuery
        ));
        list.add(buildPair(
                location,
                PeopleDataLabQuery::locationQuery
        ));
        list.add(buildPair(
                skill,
                PeopleDataLabQuery::skillQuery
        ));
        list.add(buildPair(
                education,
                PeopleDataLabQuery::educationQuery
        ));
        list.forEach(pair -> addIfNotEmpty(
                pair.getFirst(), boolQueryBuilder, pair.getSecond()
        ));
        return boolQueryBuilder;
    }

    private Pair<String, Function<String, QueryBuilder>> buildPair(
            final String value,
            final Function<String, QueryBuilder> fieldQueryFunc
    ) {
        return Pair.of(
                Optional.ofNullable(value).orElse(""),
                fieldQueryFunc
        );
    }

    private void addIfNotEmpty(
            final String value,
            final BoolQueryBuilder main,
            final Function<String, QueryBuilder> fieldQueryFunc
    ) {
        if (hasLength(value)) {
            main.must(fieldQueryFunc.apply(value));
        }
    }

    /**
     * Query.
     * @param value
     * @return query
     */
    public static QueryBuilder fullNameQuery(final String value) {
        return termQuery("full_name", value);
    }

    /**
     * Query.
     * @param value
     * @return query
     */
    public static QueryBuilder linkedinUrlQuery(final String value) {
        return termQuery("linkedin_url", normalizeUrl(value));
    }

    /**
     * Query.
     * @param value
     * @return query
     */
    public static QueryBuilder facebookUrlQuery(final String value) {
        return termQuery("facebook_url", normalizeUrl(value));
    }

    /**
     * Query.
     * @param value
     * @return query
     */
    public static QueryBuilder twitterUrlQuery(final String value) {
        return termQuery("twitter_url", normalizeUrl(value));
    }

    /**
     * Query.
     * @param value
     * @return query
     */
    public static QueryBuilder githubUrlQuery(final String value) {
        return termQuery("github_url", normalizeUrl(value));
    }

    /**
     * Query.
     * @param value
     * @return query
     */
    public static QueryBuilder jobTitleQuery(final String value) {
        return termQuery("job_title", value);
    }

    /**
     * Query.
     * @param value
     * @return query
     */
    public static QueryBuilder workEmailQuery(final String value) {
        return termQuery("work_email", value);
    }

    /**
     * Query.
     * @param value
     * @return query
     */
    public static QueryBuilder personalEmailQuery(final String value) {
        return termQuery("personal_emails", value);
    }

    /**
     * Query.
     * @param value
     * @return query
     */
    public static QueryBuilder companyNameQuery(final String value) {
        return termQuery("job_company_name", value);
    }

    /**
     * Query.
     * @param value
     * @return query
     */
    public static QueryBuilder locationQuery(final String value) {
        return termQuery("location_name", value);
    }

    /**
     * Query.
     * @param value
     * @return query
     */
    public static QueryBuilder skillQuery(final String value) {
        return termQuery("skills", value);
    }

    /**
     * Query.
     * @param value
     * @return query
     */
    public static QueryBuilder educationQuery(final String value) {
        return termQuery("education.school.name", value);
    }

    /**
     * Normalize url.
     * @param url url
     * @return url
     */
    public static String normalizeUrl(final String url) {
        return url.replace("http://", "")
                .replace("https://", "")
                .replace("www.", "");
    }
}

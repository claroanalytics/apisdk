package com.claro.apisdk.dto.people;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * People DTO.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PeopleDTO {
    /** Linkedin url.*/
    private String linkedinUrl;

    /** Profile ID.*/
    private String profileId;

    /** email.*/
    private String email;

    /** work email.*/
    private String workEmail;

    /** FullName.*/
    private String fullname;

    /** Current company.*/
    private String company;
}

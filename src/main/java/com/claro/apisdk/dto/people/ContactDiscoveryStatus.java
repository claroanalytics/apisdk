package com.claro.apisdk.dto.people;

/**
 * Result of contact discovery operation.
 */
public enum ContactDiscoveryStatus {

    /**
     *  Argument 'talentId' is invalid or unresolvable.
     */
    BAD_REQUEST,

    /** At least one provider returned valid result. */
    FOUND,

    /** None of providers returned valid result. */
    NOT_FOUND,

    /** Providers work asynchronously. */
    REQUESTED,

    /** Request failed. */
    FAILED
}

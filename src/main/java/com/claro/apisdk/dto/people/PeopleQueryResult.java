package com.claro.apisdk.dto.people;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Result of query for people info.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PeopleQueryResult {
    /** API request state.*/
    private PeopleAPIRequestState state;

    /** People DTO.*/
    private PeopleDTO people;
}

package com.claro.apisdk.dto.people;

public enum PeopleAPIRequestState {
    /** Contact requested.*/
    REQUEST,
    /** Contact successfully retrieved.*/
    OK,
    /** Contact not found.*/
    NOT_FOUND,
    /** Error while retrieving.*/
    ERROR
}

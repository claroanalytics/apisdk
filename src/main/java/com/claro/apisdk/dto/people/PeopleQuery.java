package com.claro.apisdk.dto.people;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Query for people info.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PeopleQuery {

    /** Api service code.*/
    private String apiService;

    /** People query params.*/
    private List<PeopleDTO> people;

    /** Request contact if it not exists.*/
    private boolean requestIfNotExists;
}

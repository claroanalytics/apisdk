package com.claro.apisdk.dto.people.email;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Email people.
 *
 * @author Sergey Parfenov
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PeopleEmailDTO {

    /**
     * Value email.
     */
    private String email;

    /**
     * List providers for email.
     */
    @Builder.Default
    private List<String> sources = new ArrayList<>();


}

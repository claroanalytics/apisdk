package com.claro.apisdk.dto.people;

/**
 * Result of verification.
 */
public enum VerificationResult {

    /**
     * Verified.
     */
    VERIFIED,

    /**
     * Not verified.
     */
    NOT_VERIFIED,

    /**
     * Cant verify.
     */
    CANT_VERIFY;
}

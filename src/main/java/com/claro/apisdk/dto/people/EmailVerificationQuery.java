package com.claro.apisdk.dto.people;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Query for email verification.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmailVerificationQuery {
    /** Email .*/
    private String email;
}

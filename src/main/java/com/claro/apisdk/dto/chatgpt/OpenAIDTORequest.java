package com.claro.apisdk.dto.chatgpt;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class OpenAIDTORequest {

    /**
     * User prompt.
     */
    private String userPrompt;
    /**
     * System prompt.
     */
    private String systemPrompt;
    /** ChatGPT model. */
    private String model;
    /**
     * Max tokens.
     */
    private Integer maxTokens;
    /**
     * Temperature.
     */
    private Double temperature;
}


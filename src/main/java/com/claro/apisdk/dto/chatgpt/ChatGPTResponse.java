package com.claro.apisdk.dto.chatgpt;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ChatGPTResponse {
    /**
     * Id.
     */
    private String id;
    /**
     * Object.
     */
    private String object;
    /**
     * Created.
     */
    private long created;
    /**
     * Model.
     */
    private String model;
    /**
     * Choices.
     */
    private ChatGPTChoice[] choices;
    /**
     * Usage.
     */
    private Usage usage;


    @Data
    public static class ChatGPTChoice {
        /**
         * Index.
         */
        private int index;
        /**
         * Message.
         */
        private ChatGPTMessage message;
        /**
         * Finish reason.
         */
        @JsonProperty("finish_reason")
        private String finishReason;
    }

    @Data
    public static class ChatGPTMessage {
        /**
         * Role.
         */
        private String role;
        /**
         * Content.
         */
        private String content;
    }

    @Data
    public static class Usage {
        /**
         * Prompt tokens.
         */
        @JsonProperty("prompt_tokens")
        private int promptTokens;
        /**
         * Completion tokens.
         */
        @JsonProperty("completion_tokens")
        private int completionTokens;
        /**
         * Total tokens.
         */
        @JsonProperty("total_tokens")
        private int totalTokens;
    }
}

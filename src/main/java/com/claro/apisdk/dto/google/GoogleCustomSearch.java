package com.claro.apisdk.dto.google;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;
import java.util.List;

/**
 * Google custom search.
 */
@Document(indexName = "google_custom_search")
@Data
public class GoogleCustomSearch {

    /**
     * Id.
     */
    @Id
    private String id;

    /**
     * Custom search request.
     */
    private CustomSearchRequest customSearchRequest;

    /**
     * Search.
     */
    private List<GoogleResultDTO> results;

    /**
     * Search json.
     */
    private String searchJSON;

    /**
     * Date.
     */
    @Field(type = FieldType.Date)
    private Date date;
}

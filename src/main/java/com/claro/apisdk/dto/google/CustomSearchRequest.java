package com.claro.apisdk.dto.google;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.pubdir.utils.Hash;

import java.util.Optional;

/**
 * Custom search request.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CustomSearchRequest {

    /**
     * Query.
     */
    private String query;

    /**
     * High range.
     */
    private String highRange;

    /**
     * Exact terms.
     */
    private String exactTerms;

    /**
     * Site search.
     */
    private String siteSearch;

    /**
     * Generate id.
     * @param customSearchRequest custom search request
     * @return id
     */
    public static String generateId(
            final CustomSearchRequest customSearchRequest
    ) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(customSearchRequest.getQuery());
        Optional.ofNullable(customSearchRequest.getHighRange())
                .ifPresent(stringBuilder::append);
        Optional.ofNullable(customSearchRequest.getExactTerms())
                .ifPresent(stringBuilder::append);
        Optional.ofNullable(customSearchRequest.getSiteSearch())
                .ifPresent(stringBuilder::append);
        return new Hash().md5String(stringBuilder.toString());
    }
}

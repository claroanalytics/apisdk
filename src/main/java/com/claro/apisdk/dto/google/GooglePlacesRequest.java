package com.claro.apisdk.dto.google;

import com.google.maps.model.PlaceAutocompleteType;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Google autocomplete find places request.
 *
 * @author Sergey Parfenov
 */
@Data
@Accessors(chain = true)
public class GooglePlacesRequest {

  /**
   * Text.
   */
  private String input;

  /**
   * PlaceAutocompleteType.
   */
  private PlaceAutocompleteType placeAutocompleteType;

  /**
   * Client ip.
   */
  private String ipbias;
}

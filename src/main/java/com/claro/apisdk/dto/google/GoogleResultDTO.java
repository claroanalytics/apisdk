package com.claro.apisdk.dto.google;

import lombok.Data;

/**
 * Google result dto.
 */
@Data
public class GoogleResultDTO {

    /**
     * Display link.
     */
    private String displayLink;

    /**
     * Formatted url.
     */
    private String formattedUrl;

    /**
     * Link.
     */
    private String link;

    /**
     * Mime.
     */
    private String mime;

    /**
     * Snippet.
     */
    private String snippet;

    /**
     * Title.
     */
    private String title;
}

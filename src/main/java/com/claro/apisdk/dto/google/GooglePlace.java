package com.claro.apisdk.dto.google;

import com.google.maps.model.AutocompletePrediction;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Holds google places api response.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GooglePlace {

    /** Country .*/
    private String country;
    /** Google autocomplete prediction.*/
    private AutocompletePrediction autocompletePrediction;
}

package com.claro.apisdk.dto.rabbit;

import java.util.EnumMap;
import lombok.Data;

/**
 * Social link data for user.
 *
 * @author Sergey Parfenov
 */
@Data
public class SocialLinkDto {

  /**
   * userId.
   */
  private String userId;

  /**
   * Email.
   */
  private String email;

  /**
   * Key - social network, value - link.
   */
  private EnumMap<SocialNetworkType, String> snLink =
      new EnumMap<>(SocialNetworkType.class);
}

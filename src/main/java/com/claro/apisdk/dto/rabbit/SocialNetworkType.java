package com.claro.apisdk.dto.rabbit;

/**
 * Social network type.
 *
 * @author Sergey Parfenov
 */
public enum SocialNetworkType {

  /**
   * Youtube.
   */
  YOUTUBE,

  /**
   * Github.
   */
  GITHUB,

  /**
   * Wordpress.
   */
  WORDPRESS,

  /**
   * Facebook.
   */
  FACEBOOK,

  /**
   * Foursquare.
   */
  FOURSQUARE,

  /**
   * Tumblr.
   */
  TUMBLR,

  /**
   * Google.
   */
  GOOGLE,

  /**
   * Linkedin.
   */
  LINKED_IN,

  /**
   * Instagram.
   */
  INSTAGRAM,

  /**
   * Twitter.
   */
  TWITTER,

  /**
   * Vimeo.
   */
  VIMEO,

  /**
   * Flickr.
   */
  FLICKR,

  /**
   * Blogspot.
   */
  BLOGSPOT,

  /**
   * Stackoverflow.
   */
  STACKOVERFLOW,

  /**
   * Stackexchange.
   */
  STACKEXCHANGE;
}

package com.claro.apisdk;

import com.claro.apisdk.util.RestTemplateUtil;
import lombok.Getter;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import java.net.HttpURLConnection;
import java.util.Base64;
import java.util.Date;

/**
 * Global rest api config.
 */
@Component
@Scope("singleton")
public class RestApiConfig {

    /**
     * AUTHENTICATION enpoint.
     */
    private static final String AUTHENTICATION_URL
            = "/api//public/authenticate/";

    /**
     * Client id.
     */
    @Getter
    @Value("${global.api.client.id:}")
    private String clientId;

    /**
     * Client secret.
     */
    @Getter
    @Value("${global.api.client.secret:}")
    private String clientSecret;

    /**
     * Audience.
     */
    @Getter
    @Value("${global.api.audience:}")
    private String audience;

    /**
     * Host.
     */
    @Getter
    @Value("${global.api.host:}")
    private String host;

    /**
     * Token.
     */
    private String token;

    /**
     * Get token.
     *
     * @return token
     */
    public String getToken() {
        if (token == null || token.isEmpty() || expired()) {
            token = refreshToken();
        }
        return token;
    }

    /**
     * Sets bearer token to Authentication header in HttpURLConnection.
     * @param connection HttpURLConnection
     */
    public void useAuthFor(final HttpURLConnection connection) {
        String bearer = "Bearer " + getToken();
        connection.setRequestProperty("Authorization", bearer);
    }

    /**
     * Refresh token.
     * @return token
     * @throws HttpClientErrorException e
     */
    private String refreshToken() throws HttpClientErrorException {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        JSONObject jsonBodyObject = new JSONObject();
        jsonBodyObject.put("clientId", clientId);
        jsonBodyObject.put("clientSecret", clientSecret);
        jsonBodyObject.put("audience", audience);
        HttpEntity<String> entity =
                new HttpEntity<>(jsonBodyObject.toString(), httpHeaders);
        ResponseEntity<String> responseEntity = RestTemplateUtil
                .getRestTemplate()
                .exchange(
                        host + AUTHENTICATION_URL,
                        HttpMethod.POST,
                        entity,
                        String.class
                );
        JSONObject jsonObject = new JSONObject(responseEntity.getBody());
        return jsonObject.getString("token");
    }

    /**
     * Expired.
     * @return bool
     */
    private boolean expired() {
        Base64.Decoder decoder = Base64.getDecoder();
        String[] tokenParts = token.split("\\.");
        if (tokenParts.length < 2) {
            return true;
        }
        final int millisMultiplier = 1000;
        JSONObject jsonObject = new JSONObject(
                new String(decoder.decode(tokenParts[1]))
        );
        long exp = jsonObject.getInt("exp");
        exp *= millisMultiplier;
        Date currentDate = new Date();
        Date date = new Date(exp);
        return currentDate.after(date);
    }
}

package com.claro.apisdk;

import com.claro.apisdk.util.RestTemplateUtil;
import com.pubdir.currency.dto.CurrencyConvertRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Currency Api.
 */
@Component
@Scope("prototype")
@RequiredArgsConstructor
@Slf4j
public class CurrencyApi {

    /**
     * Url path for currency api.
     */

    private String path = "/api/currency/convert";

    /**
     * Rest api Config.
     */
    private final RestApiConfig restApiConfig;

    /**
     * Retry template.
     */
    private final RetryTemplate retryTemplate;

    /**
     * Converts passed values.
     * @param fromCurrency from currency
     * @param toCurrency to currency
     * @param values values to convert
     * @return converted values
     */
    public Optional<List<Double>> convert(
            final List<Double> values,
            final String fromCurrency,
            final String toCurrency
    ) throws RestClientException {
        String token = restApiConfig.getToken();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setBearerAuth(token);
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(restApiConfig.getHost() + path);
        final int connectionTimeout = 15 * 60_000;
        CurrencyConvertRequest requestBody = new CurrencyConvertRequest(
                values,
                fromCurrency,
                toCurrency
        );
        ResponseEntity<Double[]> responseEntity = retryTemplate
                .execute(retryContext -> RestTemplateUtil
                        .getRestTemplate(connectionTimeout).exchange(
                                builder.build().toUriString(),
                                HttpMethod.POST,
                                new HttpEntity<>(requestBody, httpHeaders),
                                Double[].class
                        ));
        List<Double> doubles = Optional.ofNullable(
                responseEntity.getBody()).map(Arrays::asList)
                .orElse(Collections.emptyList());
        return Optional.of(doubles);
    }

}
